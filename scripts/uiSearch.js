/*
   Copyright 2017 Adam Łangowicz - [adam.langowicz@yahoo.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including 
   without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit
   persons to whom the Software is furnished to do so, subject to the
   following conditions:
   
   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY
   OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
   LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
   FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
   EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR
   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
   THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.

*/

var baseVar =
    document.querySelector("#search");

var baseVarOne = 
    document.querySelector("#result");

var searchInput =
    baseVar.addEventListener("input", searchQuery);

var resultButton = baseVarOne.addEventListener("click", searchQueryResult);

var dynamicDiv =
    document.createElement("div");

var detailePoster = "";
var detaileDescription = "";
    //document.querySelector("#tempPlaceHolder").style.display = 'none';

function searchQuery() {

    var api_key = 'e4039eec747d40607c73029ffb52f735';
    var query = baseVar.value;
    //console.log(baseVar.value);

    if (baseVar.value != "") {
        //document.querySelector("#tempPlaceHolder").style.display = 'block';
        //var displayWhenInputType =
        //document.querySelector("#tempPlaceHolder").innerHTML = 
        //baseVar.value;

        var xmlhttp = new XMLHttpRequest();
        var url = "https://api.themoviedb.org/3/search/movie?api_key=" + api_key +
            "&language=en-US&query=" + query +
            "&page=1&include_adult=false";

        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var myArr = JSON.parse(this.responseText);
                grabJSONObjectRespond(myArr);
            }
        };

        xmlhttp.open("GET", url, true); // async. request
        xmlhttp.send();

        function grabJSONObjectRespond(arr) {

            //dynamicDiv.innerHTML = "";
            document.querySelector("#tempPlaceHolder").innerHTML = "";
            //console.log(arr);
            var movieDetileJSONObject = arr;

            movieDetileJSONObject.results.forEach((entry) => {


                var entryTitle = document.createTextNode(entry.title);
                document.querySelector("#tempPlaceHolder").insertAdjacentHTML('beforeend','<option value="'+entryTitle.textContent +'"></option>');
                detailePoster = entry.backdrop_path;
                detaileDescription = entry.overview;
                //var option = 
                //dynamicDiv.appendChild(entryTitle);
                //console.debug(entryTitle);
                //console.debug(dynamicDiv);

            }, this);
            //document.querySelector("#tempPlaceHolder").innerHTML = dynamicDiv.textContent;
            //document.querySelector("#tempPlaceHolder").insertAdjacentHTML('beforeend','<option value="'+ dynamicDiv.textContent+'"></option>');
            //console.log(dynamicDiv.textContent, dynamicDiv);
            //console.clear();
            //console.debug(tempPlaceHolder);

            //item.replaceChild(dynamicDiv, item.lastChild)    
        }
    } else {
        //document.querySelector("#tempPlaceHolder").style.display = 'none';
        //document.querySelector("#tempPlaceHolder").innerHTML = "";
    }

}

function searchQueryResult() {
    if (baseVar.value  === null || baseVar.value == "" ) {
    } else {
        var recentQueryInputValue = document.querySelector("#search").value;
        console.log(recentQueryInputValue);
        console.log(detailePoster);
        document.querySelector("body").innerHTML = 
        `<body>
            <div class="movedDisplay">
                <div class="ui grid">
                   <div class="row">
                   
                    <div class="seven wide column">
                        <div class="ui segment">
                            <img src="https://image.tmdb.org/t/p/w500`+detailePoster+`">
                        </div>
                    </div>

                    <div class="six wide column">
                        <div class="ui segment">
                            `+detaileDescription+`
                        </div> 
                        <div class="two wide column">
                            <button class="ui right floated button teal " onclick="location.reload()">back</button>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </body>` ;

    }
}
